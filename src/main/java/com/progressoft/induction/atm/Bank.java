/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progressoft.induction.atm;

import java.math.BigDecimal;

/**
 *
 * @author user
 */
public class Bank{
    private  String accountNumber;
    private  BigDecimal balance;
    
    public Bank(String accountNumber, BigDecimal balance){
        this.accountNumber=accountNumber;
        this.balance=balance;
    }

    public String getAccountNumber(){
        return accountNumber;
    }
    
    public BigDecimal getBalance(){
        return balance;
    }
    
    public void setBalance(BigDecimal balance){
        this.balance=balance;
    }

      public void setAccountNumber(String accountNumber){
        this.accountNumber=accountNumber;
    }
    

     
}
