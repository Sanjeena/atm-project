/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progressoft.induction.atm;


import java.math.BigDecimal;


import java.util.Map;

/**
 *
 * @author user
 */
public class AtmNote {
    
    private Map<Banknote,Integer> noteMap;
    
    AtmNote(Map<Banknote, Integer> notes){
       noteMap = notes;
      
    }
    
    public BigDecimal getATMBalance(){
        BigDecimal balance=new BigDecimal(0.0);
        
		for (Map.Entry<Banknote, Integer> entry : this.noteMap.entrySet()) {
			balance = balance.add(BigDecimal.valueOf(entry.getValue()).multiply(entry.getKey().getValue()));
		}
		return balance;   
       } 
      
    	public Map<Banknote, Integer> getNoteMap() {
		return noteMap;
	}
        
        public void setNoteMap(Map<Banknote, Integer> notes) {
		this.noteMap.putAll(notes);
	}
        
        public int getAvailableNotes(Banknote banknote){
		return this.noteMap.get(banknote)==null?0:this.noteMap.get(banknote);
	}
        
   
}
