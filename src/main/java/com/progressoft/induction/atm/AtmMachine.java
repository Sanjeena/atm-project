/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progressoft.induction.atm;

import com.progressoft.induction.atm.exceptions.AccountNotFoundException;
import com.progressoft.induction.atm.exceptions.DispenseException;
import com.progressoft.induction.atm.exceptions.InsufficientFundsException;
import com.progressoft.induction.atm.exceptions.NotEnoughMoneyInATMException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author user
 */
public class AtmMachine implements BankingSystem,ATM{
    private AtmNote atmNote;
    List<Bank>listOfBank=new ArrayList<Bank>();
    Map<Banknote, Integer> notes = new HashMap<Banknote, Integer>();
        
       
    
    AtmMachine(){
        listOfBank.add(new Bank("123456789",new BigDecimal("1000.0")));
        listOfBank.add(new Bank("111111111",new BigDecimal("1000.0")));
        listOfBank.add(new Bank("222222222",new BigDecimal("1000.0")));
        listOfBank.add(new Bank("333333333",new BigDecimal("1000.0")));
        listOfBank.add(new Bank("444444444",new BigDecimal("1000.0")));
        notes.put(Banknote.FIFTY_JOD, 10);
        notes.put(Banknote.TWENTY_JOD, 20);
        notes.put(Banknote.TEN_JOD, 100);
        notes.put(Banknote.FIVE_JOD, 100);
        atmNote = new AtmNote(notes);
        
    }
   

    @Override
    public BigDecimal getAccountBalance(String accountNumber) {
        for(Bank banks:listOfBank){
            if(accountNumber.equals(banks.getAccountNumber())){
                return banks.getBalance();
            }
        }
        throw new AccountNotFoundException("Account not found");
        
    }

    @Override
    public void debitAccount(String accountNumber, BigDecimal amount) {
        BigDecimal initialAmount=getAccountBalance(accountNumber);
        BigDecimal deductedAmount=initialAmount.subtract(amount);
            for(Bank banks:listOfBank){
            if(accountNumber.equals(banks.getAccountNumber())){
              banks.setBalance(deductedAmount);
            }
        }
       
    }

    @Override
    public List<Banknote> withdraw(String accountNumber, BigDecimal amount) {
        BigDecimal divisorFive=new BigDecimal("5.0");
        BigDecimal divisorTen=new BigDecimal("10.0");
        BigDecimal num=new BigDecimal("0.0");
        List<Banknote> totalNotes=new ArrayList<Banknote>();
        Map<Banknote,Integer>returnNotes=new HashMap<Banknote,Integer>();
        
      
        if(getAccountBalance(accountNumber).equals(num)||getAccountBalance(accountNumber).compareTo(amount)<0)
        {
              throw new InsufficientFundsException("Account doesn't have enough money to withdraw");   
            } 
        
        if((amount.compareTo(num)==1)|| amount.remainder(divisorFive).equals(num) || amount.remainder(divisorTen).equals(num)){
                
             if(atmNote.getATMBalance().compareTo(amount) <0) {
                   throw new NotEnoughMoneyInATMException("ATM doesnot have enough money to dispense");  
                   }
             
                  returnNotes = getDespenseAmount(accountNumber,amount);
                  debitAccount(accountNumber,amount);
                  for(Map.Entry<Banknote, Integer> request : returnNotes.entrySet()){
                         for(int i=0;i<request.getValue();i++){
                               totalNotes.add(request.getKey());
                             }
		    }
                  reduceAtmBalance(returnNotes);
               
                  return totalNotes;
            }
        throw new DispenseException("Please enter the amount which is  multiple of 10 or 5");
                 
    }
    
      private Map<Banknote,Integer> getDespenseAmount(String accountNumber,BigDecimal amount){
           List<Banknote> bankNotes = Arrays.asList(Banknote.values());
           // sort the banknotes in descending order
           Collections.sort(bankNotes, new BankNoteComparator());
           BigDecimal noteInATM=new BigDecimal("0.0"); 
           BigDecimal num=new BigDecimal("0.0");
           int notecounter=0;
           int actualcount=0;
           Map<Banknote, Integer>result=new HashMap<Banknote, Integer>();
           
           
           for(Banknote note : bankNotes){
	         noteInATM = note.getValue();
                 //check whether the note is available or not if not continue
                 if(atmNote.getAvailableNotes(note) <=0){
                  continue;
                 }
                 //check whether the required amount is greater than the value of banknote
                 if(amount.compareTo(noteInATM) >=0){
                  
                  notecounter=(amount.intValue())/(noteInATM.intValue());
                 
                  
                 if(notecounter<=0){
                     continue;
                 }
                 
                 if(notecounter<=atmNote.getAvailableNotes(note)){
                     actualcount=notecounter;
                 }
                 else{
                     actualcount=atmNote.getAvailableNotes(note);
                 }
                 
                amount=amount.subtract(BigDecimal.valueOf(actualcount).multiply(noteInATM));
                result.put(note, actualcount);
                      
                if(amount.compareTo(num)==0){
                  break;
                 }
               }
            }
       return result;
                
      }
      
         private void reduceAtmBalance(Map<Banknote,Integer>totalbanknote){
             //Atm notes are deducted after the completion of withdraw
           for(Map.Entry<Banknote, Integer> request : totalbanknote.entrySet()){
                
		atmNote.getNoteMap().put(request.getKey(), atmNote.getNoteMap().get(request.getKey())-request.getValue());
           }
        }
}
