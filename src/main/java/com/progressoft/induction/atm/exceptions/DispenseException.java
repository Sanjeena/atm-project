/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progressoft.induction.atm.exceptions;

/**
 *
 * @author user
 */
public class DispenseException extends RuntimeException  {
     public DispenseException(String message){
        super(message);
    }
}
