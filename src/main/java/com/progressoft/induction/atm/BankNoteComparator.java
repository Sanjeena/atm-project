/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progressoft.induction.atm;

import java.util.Comparator;

/**
 *
 * @author user
 */
public class BankNoteComparator implements Comparator<Banknote>{

    @Override
    public int compare(Banknote banknote1, Banknote banknote2) {
         return banknote2.getValue().compareTo(banknote1.getValue());
    }
    
}
